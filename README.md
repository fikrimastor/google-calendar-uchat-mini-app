## Create Mini-Apps 2.0

> **Example**
>
> Calendar Application API, **[Google Calendar](https://developers.google.com/calendar)** will be used as an example to show step by step how to use Mini App Version 2.0 with OAuth Authentication features.
>

![Setup App](img/setup-mini-app.jpeg "Setup mini app, step by step")

To create an app, follow steps 1 to 6 in the above screenshot.

On the editing page (left navigation) > Settings > Edit button, list a title, description, logo, cover page, YouTube video id and it will appear like this in the mini-app store:

![Setting App](img/setting-mini-app.png "Setting mini app, the view reflected in Mini App Store")

### Flows

This block is for setting the flow for mini app actions. Each flows created will be used within action block setting.

It is the similar view when you setup the flow for your chatbot, but with some limited function.

This flow objectives only focus to sent a request to third party application (API), or make some calculations based on some requirement of third party API.

##### Create A Flow

It is the same structure you see when you are building your bot flow. But the different is in mini app, it's only related with the data processing of the given input by bot user.

Either you want to make some calculation or sent some data by using external request to third party services.

For this example, we will use external request to sent data from bot to the Google Calendar server.

The purpose is to make an event and invite the bot user to the event, thus finally give them reminder or and email to accept the invitation and also give some url for user add the event to their own calendar.

![Flows Editing](img/flows-editing-mode.jpeg "Flow view while editing in editing mode")
### Auth

This block is for authentications setting of your app.

From this section, you should notice that we have two Auth Type:
- Using API Key
- Using OAuth

<!-- [![Flows Editing](img/flows-editing-mode.jpg "Flow view while editing in editing mode")] -->

#### Setup using API Key

If you are using API Key, you may use inputs field to define the important field that required for making API calls, like bearer token, api key or else.

Based on screenshot, it's an example on how we use API Key given by Razorpay payment gateway and make the field for user key in their credentials.

![API Key](img/api-key-setting-page.jpeg "API Key setting page")

#### Setup using OAuth

When developing mini app for Google services, they recommend to use OAuth for authentication process. This will return an access token that will be saved in system field in UChat server.

![OAuth Setting](img/OAuth-setting-page.jpeg "OAuth setting page")

Based on the screenshot above, after you click edit button, you will see that there is two information that are required:
- Client ID
- Client Secret

The first thing that you need to setup is the app in **[Google Developer Console](https://console.developers.google.com/)**.

##### Enable The API Library

When you have created new project, click left navigation menu > APIs & Services > Library.

The first thing you need to do is activate the Library. In this example, we are using Google Calendar, so we need to enable Google Calendar API in the library.

As per screenshot, you will see on how to search the library and also how to enable it.

![API Library](img/api-library-1.png "API Library Page 1")

![API Library](img/api-library-2.jpeg "API Library Page 2")

![API Library](img/api-library-3.jpeg "API Library Page 3")


##### Obtain Credentials

In this app you will obtain two credentials: 
- Client ID
- Client Secret

After you successfully enabled the library, you will be redirected to the overview page of Google Calendar Library. On left navigation menu > Credentials.

Or At the top right of this page, click on Create Credentials button.

![API Library](img/api-library-3.jpeg "API Library Page 3")

Select which API you will be using, in this case Google Calendar API.

Under data accessing option, choose User Data.

![Credential Page 1](img/credentials-step-1.jpeg "Credential Page 1")

For scopes, it's optional. But personally, if I know this project only for specific API, I will set it straight away.

So, since it's optional, you may set the scope when you want to setup the OAuth Screen Consent later.

For example use case, for now we want to make a mini app for google calendar to give reminder to the user when the upcoming scheduled meeting or events.

You always need to refer Google Documentation on API Referrance, **[Google Calendar for Developers](https://developers.google.com/calendar/api/v3/reference)**.

![Credential Scope](img/credentials-step-2.jpeg "Credential Scope")

We will use **insert** method. Scroll down until you see Authorization headings. There is 2 type of scope:

Non-sensitive scopes:

- https://www.googleapis.com/auth/userinfo.email
- https://www.googleapis.com/auth/userinfo.profile
- openid

Sensitive scopes
- https://www.googleapis.com/auth/calendar
- https://www.googleapis.com/auth/calendar.events

Next, for OAuth Client ID, you may choose Web Application.

After that, you can give a name for this credentials. It's just a label for your referrance.

![OAuth Client ID](img/oauth-client-id.jpeg "OAuth Client ID")

For Authorized Javascript Origins sections, you need to put "https://www.uchat.com.au".

![Authorized Javascript Origins](img/javascript-origin.jpeg "Authorized Javascript Origins")

While Authorize redirect URIs, you need to go back to your Mini App in UChat dashboard. In the left menus, click on "Auth" tab.

Make sure the Auth Type is OAuth V2.

Under Step 1 OAuth v2 Config, click on edit button.

Then, you can see the redirect url generated at the top of the popup window, copy the OAuth Redirect URL and paste it in Authorize Redirect URIs in Google Console Dashboard.

![OAuth Redirect URL](img/oauth-redirect-url.jpeg "OAuth Redirect URL")

![Authorize redirect URIs](img/authorize-url.jpeg "Authorize redirect URIs")

Click Done button and then copy the credentials given by the Google.

##### OAuth Consent Screen

Before you make any API request to obtain the access token, you need to configure this OAuth Consent Screen.

In the Google Console Dashboard, under left menus, click on OAuth Consent Screen menus. Then, click edit app.

![Authorize redirect URIs](img/authorize-url.jpeg "Authorize redirect URIs")

In this page, you will see the details of your app information. You may update the details according to your organisation or app that you want to integrate. In this case, the application is UChat.

You may publish this app or also you can also use this app without publish and only add selected email as test user. This may relevant if you want to allow only individuals within your organisation to have access to the API.

There is 4 step in this editor:

- App information (Consent Screen)
- Scope 
- Test User
- Summary

**App information**, it was the information that will be view by the user of the app when they want to install or authorizing their account to use the service or app that you created.

**Scopes** express the permissions you request users to authorize for your app and allow your project to access specific types of private user data from their Google Account.

**Test User**, current publishing status is set to "Testing", only test users are able to access the app. Allowed user cap prior to app verification is 100, and is counted over the entire lifetime of the app. You may add up to 100 user email for your own organisation use. But if you plan to open this app for public use, then you need to publish this app and follow the review process that required by the Google.

**Summary**, this is the final page that will show all the important detail regarding your app. And it show that your app finally ready to use.

Ok, now you have done all the details needed to create new project in Google Developer Console.

Now, head back to mini app, there is several section that you need to take action to ensure the bridge/integration between the newly created project in Google Developer Console and UChat is communicate each other.

Go to Auth menus (left side) and under OAuth v2 Config, click edit button.

You will notice section separated:

1. Application Credentials
2. Authorize URL
3. Get Access Token
4. Refresh Access Token

##### Application Credentials

This is where you paste in Client ID and Client Secret field respectively, that you obtain from Google Developer Console Dashboard.

![Credentials](img/credentials.jpeg "Credentials")

![Credentials](img/credentials-mini-app.jpeg "Credentials")

##### Authorize URL

This section where the first interaction between bot owner (you), need to sign in using their google account.

![OAuth Setting](img/OAuth-setting-page.jpeg "OAuth setting page")

1. Make sure the Auth Type is OAuth V2.
2. Click Edit.

![Authorize URL Step 1](img/authorize-url-request-step1.jpeg "Authorize URL Step 1")

1. Under Authorize URL section, click edit button.

![Authorize URL Step 2](img/authorize-url-request-step2.jpeg "Authorize URL Step 2")

1. Set the request as GET, and the url is: https://accounts.google.com/o/oauth2/auth

You may refer on this Google Documentation for more explanation:
**[Set authorization parameters](https://developers.google.com/identity/protocols/oauth2/web-server#httprest_1)**

In the link above, Google briefly show the step on how to use their API. Just make sure you are on the tab HTTP/REST documentation.

After you click save, you must define the scope. You may refer Google Documentation to see which scope that mini app need for every request.

For example, while we building the "Add Events" action for this Google Calendar mini app, we need to include the scope state by Google in this page:  **[Authorization](https://developers.google.com/calendar/api/v3/reference/events/insert#auth)**

![Authorization Scope](img/authorization-scope.jpeg "Authorization Scope")

##### Get Access Token

This section will do token exchanging after the bot owner (you) successfully connect their google account into UChat.

When you click edit request, it will show the same interface that you will see when you want to do external request.

In the URL section you must fill in this url: https://www.googleapis.com/oauth2/v4/token

Method: POST

Under body section, there is no additional data that you need to put, but ensure the parameters and value is same like screenshot below.

![Access Token](img/get-access-token.jpeg "Access Token")

Or you may refer to Google Documentation for your referrance: **[Exchange authorization code](https://developers.google.com/identity/protocols/oauth2/web-server#httprest_3)**

>Google responds to this request by returning a JSON object that contains a short-lived access token and a refresh token. Note that the refresh token is only returned if your application set the access_type parameter to offline in the initial request to Google's authorization server. - **[Google Developer Documentation](https://developers.google.com/identity/protocols/oauth2/web-server#httprest_3)**

##### Refresh Token

Previously, you have succesfully setup the access token, now you need to setup refresh token request.

Refresh token is the token return when you set the parameter of access_type at the first request when you want to connect into your google account. It must be value offline.

![Access Type](img/access_type.jpeg "Access Type")

Google elaborate the use of Refresh Token as below:

> A token that you can use to obtain a new access token. Refresh tokens are valid until the user revokes access. Again, this field is only present in this response if you set the access_type parameter to offline in the initial request to Google's authorization server.

So in return of the Authorize Request, google will response with this refresh token to make sure the UChat server can fetch up new access token every time when the access token is expired.

The bot owner (you) no need to request new access token manually if the token was expired. UChat will handle it with the refresh token.

When you click edit request for refresh token, you must ensure the details as below:

Request URL : https://oauth2.googleapis.com/token
Method: POST

![Refresh Token](img/refresh-token.jpeg "Refresh Token")

### Action

This block will show how to setup the action for mini app.

![Action Step 1](img/action_1.jpeg "Action Step 1")

1. Create new action
2. Assign which flow that you want this action do
3. Edit the action

When you click edit, you will see several field is required.

Name: The name for the action given, must be unique and different with other actions.

Title: The name for action, the bot owner will see this name in public view.

Inputs: The details that the bot owner must include in order to make sure the request made to third party application is successful. Depends on the input, either it is required or not.

Outputs: The response given after the request made to the third party is success or failed. This information given will be saved in user custom fields.

![Edit Action](img/edit-action.jpeg "Edit Action")

### Trigger

This will elaborate more on how to make trigger and how it relate with the action and flow we setup earlier.

So, trigger is depends on the mini app or integration that you want to setup. In many case, not every mini app really need to use trigger, some are usefull if we setup the trigger in the bot.

This will related with your objective on how this mini app will give a response to bot user after specific action.

But for this trigger, we'll show some example by using Razorpay Payment Gateway.

The situation is when the bot user have succesfully made a payment, the trigger will be triggered by the data sent by payment gateway to inform the payment have succefully verified.

In the simplest form, trigger will more likely be use to give updated information or record when there is some new data updated in third party application (Razorpay).

![Create Trigger](img/create-trigger.jpeg "Create Trigger")

![Edit Trigger](img/edit-trigger.jpeg "Edit Trigger")

### One Time URL

This section will use Razorpay payment gateway as example.

In simple words, one time url can be use as callback url, receive payloads, inbound webhook but within limited time.

After the time limit reached, the url no longer active or accessible.

In Razorpay use case, we want to capture the payload after the bot user successfully make a payment. To ensure this use case is working, we need to create new flow namely as "Payment Completion" to make use the trigger feature in mini app.

The flow Payment Completion will fire the selected trigger that we setup in previous section.

![One Time URL Step 1](img/one-time-url-1.jpeg "One Time URL Step 1")
![One Time URL Step 2](img/one-time-url-2.jpeg "One Time URL Step 2")
![One Time URL Step 3](img/one-time-url-3.jpeg "One Time URL Step 3")
![One Time URL Step 4](img/one-time-url-4.jpeg "One Time URL Step 4")
![One Time URL Step 5](img/one-time-url-5.jpeg "One Time URL Step 5")

### Sources

This block will show how to setup the source for mini app.

Basically, it will have 2 type of source, namely Dynamic and Static.

Dynamic, we can get some data using external request. This will return data that unique own by that entities.

In this case, for Google Calendar API, the events created might have data that will keep being use everytime we make a request.

In simple word, they might be some data that we have set in the account before, and that data was one of required parameters for any other request that we want to use in next request.

##### Dynamic source

![Dynamic source](img/dynamic-source.jpeg "Dynamic source")

##### Static source

![Static source](img/static-source.jpeg "Static source")

###### Save & Test

